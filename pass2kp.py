#!/usr/bin/env python3

"""
pass2kp.py
Convert a pass datapase to KeePass format.

Copyright 2015 Francis Thérien
Released under the terms of the MIT license, see LICENSE.txt for details.
"""

import os

from os import path

import subprocess

from xml.etree import ElementTree as ET

from xml.dom import minidom

import uuid

import time

import io

import argparse


class PassDb():
    def __init__(self, pass_dir):
        self.pass_dir = pass_dir

    def _is_hidden(self, filepath):
        filepath = path.relpath(filepath, self.pass_dir)
        while filepath:
            head, tail = path.split(filepath)
            if tail.startswith('.') and tail not in ['.', '..']:
                return True
            filepath = head
        return False

    def walk(self, exclude_hidden=True):
        """Walk all entries in a similar way to os.walk"""
        for dirpath, dirnames, filenames in os.walk(self.pass_dir):
            reldirpath = path.relpath(dirpath, self.pass_dir)
            if reldirpath == '.':
                reldirpath = ''

            records = [self.get_record(path.join(reldirpath, fname))
                       for fname in filenames if not self._is_hidden(fname)
                       ]

            reldirnames = [path.join(reldirpath, name)
                           for name in dirnames if not self._is_hidden(name)
                           ]

            if (not exclude_hidden) or (not self._is_hidden(dirpath)):
                yield reldirpath, reldirnames, records

    def get_record(self, recpath):
        return Record(recpath, self.pass_dir)


class Record():
    def __init__(self, rec_path, db_path):
        self.rec_path = rec_path
        self.db_path = db_path

    @property
    def abspath(self):
        return path.join(self.db_path, self.rec_path)

    @property
    def decrypted(self):
        return subprocess.check_output(
            ("gpg", "-d", "--quiet", "--use-agent", "--batch", self.abspath)
            ).decode('utf-8')

    @property
    def decrypted_password(self):
        return self.decrypted.split('\n')[0]

    def __repr__(self):
        return ('Record(%s)' % self.rec_path)


class XmlDb():
    def __init__(self):
        self.pwlist = ET.Element('pwlist')
        self.entries = []

    @staticmethod
    def time2kp(etime):
        """Convert time in seconds from epoch to KP format"""
        fmt = '%Y-%m-%dT%H:%M:%S'
        return time.strftime(fmt, time.localtime(etime))

    def add_entry(self, record):
        groupname, titlename = path.split(record.rec_path)
        treename, groupname = path.split(groupname)
        if not groupname:
            groupname = 'General'
            treename = ''

        # Remove .gpg extension
        titlename = path.splitext(titlename)[0]

        dec = record.decrypted

        entry = ET.SubElement(self.pwlist, 'pwentry')

        if treename:
            ET.SubElement(entry, 'group', tree=treename).text =\
                groupname
        else:
            ET.SubElement(entry, 'group').text = groupname

        ET.SubElement(entry, 'title').text = titlename
        ET.SubElement(entry, 'username').text = ''
        ET.SubElement(entry, 'url').text = ''
        ET.SubElement(entry, 'password').text = dec.split('\n')[0]
        ET.SubElement(entry, 'notes').text = ''.join(dec.split('\n')[1:])
        ET.SubElement(entry, 'uuid').text = uuid.uuid4().hex
        ET.SubElement(entry, 'image').text = '0'

        ET.SubElement(entry, 'creationtime').text = \
            self.time2kp(path.getmtime(record.abspath))

        ET.SubElement(entry, 'lastmodtime').text = \
            self.time2kp(path.getmtime(record.abspath))

        ET.SubElement(entry, 'lastaccesstime').text = \
            self.time2kp(path.getatime(record.abspath))

        ET.SubElement(entry, 'expiretime', expires='false').text = \
            '2999-12-28T23:59:59'

        self.entries.append(entry)
        return entry

    def dump(self, file):
        """Write XML to file.

        file: Open binary file, encoding is handled in this function.

        """

        buf = io.StringIO()
        tree = ET.ElementTree(self.pwlist)
        tree.write(buf,
                   encoding='unicode',
                   xml_declaration=True,
                   method='xml',
                   short_empty_elements=False
                   )

        buf.seek(0)
        mdxml = minidom.parseString(buf.read())
        file.write(mdxml.toprettyxml(encoding='utf-8'))


def main():
    parser = argparse.ArgumentParser(
        description='Convert pass database to KeePass',
        epilog='Example: pass2kp.py ~/.password-store ~/passwords.xml'
        )

    parser.add_argument('input', type=str,
                        help='pass root directory')
    parser.add_argument('output', type=argparse.FileType('wb'),
                        help='output file path')

    args = parser.parse_args()

    db = PassDb(args.input)
    xml = XmlDb()
    for dp, dns, recs in db.walk():
        for rec in recs:
            xml.add_entry(rec)

    xml.dump(args.output)
    args.output.close()

if __name__ == "__main__":
    main()
