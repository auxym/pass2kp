pass2kp.py
========================================================

A small script to convert a [pass](http://www.passwordstore.org) database to
[KeePass](http://keepass.info) format. Currently, the only supported output format is
[KeePass 1.x XML](http://keepass.info/help/base/importexport.html#xml) which,
surpringly, is only supported for imports in KeePass 2.x.

Direct output to .kdb could be implemented using, e.g. python-keepass. Pull
requests welcome.

## Requirements

* Python 3.x needs to be installed
* GPG (with a running agent)

## Usage

```
usage: pass2kp.py [-h] input output

Convert pass database to KeePass

positional arguments:
  input       pass root directory
  output      output file path

optional arguments:
  -h, --help  show this help message and exit

Example: pass2kp.py ~/.password-store ~/passwords.xml
```

## Incompatibilities

Since there is no straightforward way to obtain file creation time in linux,
the creation time in the output KeePass file is set as the last modification
time (mtime).

Since pass doesn't have named fields (except for the password, which is the
first line in the file), all other info in the pass file is entered in the
"notes" KeePass field.
